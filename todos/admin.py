from django.contrib import admin
from django.db import models, TodoList

# Register your models here.
class TodoList(models.Model):
    pass


admin.site.register(TodoList, TodoListAdmin),
